+  Twitter App (No Scaffolding)
    +  Zurb or Bootstrap
    +  HAML Templating
    +  Devise Authentication
    +  Post/Delete Status
    +  Feed of recent posts ordered by date


Zurb Navbar example:
    %nav.top-bar
      %ul.title-area
        %li.name
          %h1=link_to "Twitter-Like App", root_path
        %li.toggle-topbar.menu-icon
          %span=link_to "Menu", "#"
      %section.top-bar-section
        %ul.right
          %li.divider
          %li.active=link_to "Main Item 1", "#"
          %li.divider
          %li=link_to "Main Item 2", "#"
          %li.has-dropdown
            =link_to "Main Item 3", "#"        
            %ul.dropdown
              %li.has-dropdown
                =link_to "Dropdown Level 1a", "#"
                %ul.dropdown
                  %li
                    %label="Dropdown Level 2 Label"
                  %li=link_to "Dropdown Level 2a", "#"
              %li=link_to "Dropdown Level 1b"