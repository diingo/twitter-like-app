class User < ActiveRecord::Base
  has_many :posts
  validates_uniqueness_of :login, :on => :update
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates_confirmation_of :password
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
