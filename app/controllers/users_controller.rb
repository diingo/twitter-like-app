class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def show
  end

  def update
    # this didn't work.. created registrations controller to override devise
    @user.update_attributes(user_params)
    if @user.save
      redirect_to @user
    else
      render action: 'edit'
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit!
    end

end
